import Header from "@/components/header"
import Banner from "@/components/banner"
import ImgLink from "@/components/imgLink"
import ApplicationFunction from "@/components/applicationFunction"
import Opinions from "@/components/opinions"
import Contacts from "@/components/contacts"
import Faq from "@/components/faq"
import Footer from "@/components/footer"

import Info from "@/data/index"

const Index = () => {
    return (
        <>
            <Header {...Info.header}/>
            <Banner {...Info.banner}/>
            <ImgLink {...Info.imgLink}/> 
            <ApplicationFunction {...Info.applicationFunction}/>
            <Opinions {...Info.opinions}/>
            <Contacts {...Info.contacts}/>
            <Faq {...Info.faq}/>
            <Footer {...Info.footer}/>
        </>
    )
}
export default Index