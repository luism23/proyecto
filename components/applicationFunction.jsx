import Img from "@/components/Img"

const Application = ({ title, subTitle, text, img }) => {
    return (
        <>
            <div className="applicationFunction-application flex flex-align-center">
                <div className=" flex-5">
                    <Img
                        src={img}
                        classNameImg=""
                        className=""
                    />
                </div>
                <div className=" flex-7">
                    <h5 className="font-24 font-w-700 font-montserrat color-red-lite m-b-7">
                        {subTitle}
                    </h5>
                    <h3 className="color-blue-dark font-40 font-montserrat font-w-700 m-b-7">
                        {title}
                    </h3>
                    <p className="m-0 color- font-24 color-gray-2 font-montserrat font-w-500">
                        {text}
                    </p>
                </div>
            </div>
        </>
    )
}


const Index = ({ title = "", application = [] }) => {
    return (
        <>
            <div>
                <div className="">
                    <h1 className="flex flex-justify-center font-44 font-w-600 font-poppins color-black">
                        {title}
                    </h1>
                </div>
                <div className=" container-2">
                    {
                        application.map((e, i) => {
                            return (
                                <Application
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }

                </div>
            </div>        
        </>
    );
};
export default Index;