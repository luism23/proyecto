import Link from "next/link";

import Img from "@/components/Img"
import Bg from "@/components/Bg"

const AppLink = ({ subTitle, title, img, link = "", className = "" }) => {
    return (
        <>
            <div className="flex-6 flex-gap-25">
                <Link href={link}>
                    <a className={`flex  border-radius-50  p-v-6 p-h-35 ${className}`}>
                        <div className="flex flex-nowrap flex-align-center">
                            <Img
                                src={img}
                                classNameImg="m-r-16 width-50"
                            />
                            <div className="">
                                <h5 className="font-18 font-montserrat font-w-600 color-white">{subTitle}</h5>
                                <h3 className="font-36 font-montserrat font-w-700 color-white">{title}</h3>
                            </div>
                        </div>
                    </a>
                </Link>
            </div>
        </>
    )
}


const Index = ({ imgBg = "", text = "", title = "", appLink = [] }) => {
    return (
        <>
            <div className="flex pos-r">
                <Bg
                    src={imgBg}
                    capas={[
                        {
                            background: "var(--blue-dark)",
                            opacity: .4
                        }
                    ]}
                />
                <div className="container-2 p-b-50 ">
                    <div className=" p-t-99 text-center">
                        <h1 className="font-40 font-w-700 font-montserrat color-white ">
                            {title}
                        </h1>
                        <p className="m-0 m-v-28 font-24 font-w-500 font-montserrat color-white">{text}</p>
                        <div className="m-b-64 boder-1 boder-white">
                            <input className="m-r-33 p-v-14 p-l-10 contacts-input font-montserrat font-24 font-w-500 bg-transparent color-white border-white border-solid" type="Email" placeholder="Email" />
                            <input className=" bg-red-lite color-white border-radius-50 border-0 width-180 p-v-14 p-h-29 font-montserrat font-22 font-w-700" type="submit" placeholder="Contact Me" />
                        </div>
                    </div>


                    <div className="flex flex-gap-25 flex-justify-between ">
                        {
                            appLink.map((e, i) => {
                                return (
                                    <AppLink
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                    </div>
                    <div className="width-p-100 p-b-76"></div>
                </div>
            </div>
        </>
    );
};
export default Index;