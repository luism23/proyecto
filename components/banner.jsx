import Link from "next/link"

import Bg from "@/components/Bg"
import Img from "@/components/Img"


const AppLink = ({ subTitle, title, img, link = "", className = "" }) => {
    return (
        <>
        <div className="flex-6 flex-gap-25">
            <Link href={link}>
                <a className={`flex  border-radius-50  p-v-6 p-h-35 ${className}`}>
                    <div className="flex flex-nowrap flex-align-center">
                        <Img
                            src={img}
                            classNameImg="m-r-16 width-50"
                        />
                        <div className="">
                            <h5 className="font-18 font-montserrat font-w-600 color-white">{subTitle}</h5>
                            <h3 className="font-36 font-montserrat font-w-700 color-white">{title}</h3>
                        </div>
                    </div>
                </a>
            </Link>
            </div>
        </>
    )
}

const Index = ({ title = "", subTitle = "", appLink = [], imgBg, img }) => {
    return (
        <>
            <div className="banner flex container p-h-15 pos-r width-p-100 height-vh-min-100">
                <Bg
                    src={imgBg}
                    capas={[
                        {
                            background: "var(--black-lite)",
                            opacity: .6
                        }
                    ]}
                />
                <div className="container flex">
                    <div className="flex-6 flex flex-align-end">
                        <div className="flex">
                            <h1 className="font-poppins font-w-700 font-58 color-white m-b-26">
                                {title}
                            </h1>
                            <h3 className="font-poppins font-w-500 font-24 color-white-2 m-b-34">
                                {subTitle}
                            </h3>
                            <div className="flex flex-gap-25 flex-justify-between m-b-120 width-p-100">
                                {
                                    appLink.map((e, i) => {
                                        return (
                                            <AppLink
                                                key={i}
                                                {...e}
                                            />
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    <div className="flex-6">
                        <Img
                            src={img}
                            classNameImg=" height-vh-max-90"
                        />
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index