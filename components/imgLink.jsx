import Link from "next/link";


import Img from "@/components/Img"


const LinkImg = ({ link = "", title = "", img }) => {
    return (
        <>
            <div className="flex flex-3 pos-r">
                <div className="flex ">
                    <Img
                        src={img}
                        classNameImg=" width-394  height-407 border-radius-20 z-index--2 pos-r"
                        className="pos-r flex border-radius-20"
                        capas={[
                            {
                                background: "var(--black)",
                                opacity: .6,
                                borderRadius:20/16+"rem",
                            }
                        ]}
                    />
                </div>
                <Link href={link}>
                    <a className="font-30 font-w-600 font-poppins color-white pos-a bottom-27 left-0 right-0 m-auto text-center">{title}</a>
                </Link>

            </div>

        </>
    )

}
const Index = ({ linkImg = [] }) => {
    return (
        <>
            <div className=" flex container p-h-15 m-t-168 m-b-122">
                {
                    linkImg.map((e, i) => {
                        return (
                            <LinkImg
                                key={i}
                                {...e}
                            />
                        )
                    })
                }
            </div>

        </>
    );
};
export default Index;
