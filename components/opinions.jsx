import Img from "@/components/Img"

const ContentInfo = ({ text="",img="",textUser="",subTextUser=""}) => {
    return (
        <>
            <div className="flex flex-4 flex-gap-15 border-solid border-1 border-white-lite p-h-43 p-v-30">
                <p className="font-18 font-montserrat font-w-500 color-gray ">
                    {text}
                </p>
                <div className="m-h-auto text-center">
                    <img src="/image/user.png" alt="" />
                    <h4 className="font-22 font-montserrat font-w-700 color-red-lite">
                        {textUser}
                    </h4>
                    <h6 className="font-18 font-montserrat color-blue-dark-2 ">
                        {subTextUser}
                    </h6>
                </div>
            </div>
        </>
    )
}



const Index = ({ title="",contentInfo=[]}) => {
    return (
        <>
            <div className="m-v-40">
                <h1 className="flex flex-justify-center color font-40 font-w-700 font-montserrat color-blue-dark m-v-51 ">
                    {title}
                </h1>
                <div className="container-2 flex flex-gap-15">
                    {
                        contentInfo.map((e,i) => {
                            return(
                                <ContentInfo
                                key={i}
                                {...e}
                                />
                            )

                        })
                    }

                </div>
            </div>
        </>
    );
};
export default Index;