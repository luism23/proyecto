import Link from "next/link";

import Img from "@/components/Img"

const FaqInfo = ({ title = "", img = "", link = "", text = "", textLink = "" }) => {
    return (
        <>
            <div className="flex-6 flex-gap-42">
                <div className="flex flex-nowrap p-t-28  p-b-12 m-b-42 border-solid border-1 border-white-lite">
                    <div className="width-87 text-center">
                        <img className="" src={`/image/${img}`} alt="" />
                    </div>
                    <div className="width-p-100">
                        <h2 className="font-25 font-w-700 font-montserrat color-blue-dark m-b-15">
                            {title}
                        </h2>
                        <p className="m-0 color-gray-2 font-18 font-w-500 font-montserrat m-b-15 m-r-15">
                            {text}
                        </p>
                        <Link href={link}>
                            <a className="color-red-lite font-18 font-w-500 font-montserrat">{textLink}</a>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
}


const Index = ({ title = "", faqInfo = [] }) => {
    return (
        <>
            <div className="">
                <div>
                    <h1 className="m-t-50 m-b-48 font-40 font-w-700 font-montserrat color-blue-dark flex flex-justify-center">
                        {title}
                    </h1>
                </div>
                <div className="container-2 flex flex-gap-42">
                    {
                        faqInfo.map((e, i) => {
                            return (
                                <FaqInfo
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
            </div>
        </>
    );
};
export default Index;