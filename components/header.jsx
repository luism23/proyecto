import Img from "@/components/Img"

const Index = ({title}) =>{
    return  (
        <>
        <header className="header pos-f top-0 left-0 width-p-100 p-v-15 z-index-50 ">

            <div className="container flex p-h-15">
                <div className="flex-6 flex flex-justify-left">
                    <Img
                    src="logo-header.png"
                    className=""

                    />
                </div>
                <div className="flex-6 flex flex-justify-right">
                    <span className=" color-white-2 font-18 font-montserrat font-w-600">
                        {title}
                    </span>
                </div>
            </div>
        </header>
        </>
    )
}
export default Index