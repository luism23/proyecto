export default {
    header: {
        title: "Privacy Policy",
    },

    banner: {
        imgBg: "banner.png",
        title: " Why stay hungry when you can order from DialEats",
        subTitle: "Download the bella onoje’s food app now on",
        appLink: [
            {
                img: "app-store.png",
                subTitle: "Download on the",
                title: "App Store",
                link: "/",
                className: "bg-red-lite",
            },
            {
                img: "play-store.png",
                subTitle: "Andriod App On",
                title: "Google Play",
                link: "/",
                className: "border-white border-1 border-solid ",
            },
        ],
        img: "menu-movil.png",
    },
    imgLink: {
        linkImg: [
            {
                img: "harare-1.png",
                link: "/",
                title: "Harare",
            },
            {
                img: "bulawayo-2.png",
                link: "/",
                title: "Bulawayo",
            },
            {
                img: "Mutare-3.png",
                link: "/",
                title: "Mutare",
            },
            {
                img: "Chinhoyi-4.png",
                link: "/",
                title: "Chinhoyi",
            },
        ],

    },
    applicationFunction: {
        title: "How the app works",
        application: [
            {
                img: "logo-1.png",
                subTitle: "Create an account",
                title: "Create/login to an existing account to get started",
                text: "An account is created with your email and a desired password",
            },
            {
                img: "logo-2.png",
                subTitle: "Explore varieties",
                title: "Shop for your favorites meal as e dey hot.",
                text: "Shop for your favorite meals or drinks and enjoy while doing it.",
            },
            {
                img: "logo-3.png",
                subTitle: "Checkout",
                title: "When you done check out and get it delivered.",
                text: "When you done check out and get it delivered with ease.",
            },
        ]
    },
    opinions: {
        title: "What others are saying",
        contentInfo: [
            {
                text: "Completely beautiful website and amazing support! This is my second website from this author and I love both of the sites so much and she has helped me so well when I needed it!",
                textUser: "Happy User",
                subTextUser: "tempy.club",
            },
            {
                text: "Completely beautiful website and amazing support! This is my second website from this author and I love both of the sites so much and she has helped me so well when I needed it!",
                textUser: "Happy User",
                subTextUser: "tempy.club",
            },
            {
                text: "Completely beautiful website and amazing support! This is my second website from this author and I love both of the sites so much and she has helped me so well when I needed it!",
                textUser: "Happy User",
                subTextUser: "tempy.club",
            },
        ]
    },
    contacts: {
        imgBg: "contacts.png",
        title: "Contact Us",
        text: "Available on your favorite store. Start your premium experience now",
        appLink: [
            {
                img: "app-store.png",
                subTitle: "Download on the",
                title: "App Store",
                link: "/",
                className: "bg-red-lite",
            },
            {
                img: "play-store.png",
                subTitle: "Andriod App On",
                title: "Google Play",
                link: "/",
                className: "border-white border-1 border-solid ",
            },

        ],
    },
    faq: {
        title: "FAQ",
        faqInfo: [
            {
                img: "logofaq.png",
                title: "How to add a booking Source",
                text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                textLink: "READ MORE",
                link: "/",
            },
            {
                img: "logofaq.png",
                title: "Rate Plan Settings",
                text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                textLink: "READ MORE",
                link: "/",
            },
            {
                img: "logofaq.png",
                title: "Ordering Food",
                text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                textLink: "READ MORE",
                link: "/",
            },
            {
                img: "logofaq.png",
                title: "Rate Plan Settings",
                text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                textLink: "READ MORE",
                link: "/",
            },
        ]
    },
    footer: {
        imgLogo: "footer-logo.png",
        title: "Copywright 2021 Dialeats.com ",
        linkRs: [
            {
                link: "/",
                img: "facebook.png",
            },
            {
                link: "/",
                img: "facebook.png",
            },
            {
                link: "/",
                img: "facebook.png",
            },
        ]


    }

}